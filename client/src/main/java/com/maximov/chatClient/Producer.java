package com.maximov.chatClient;

import org.apache.activemq.*;

import javax.jms.*;
import java.util.Scanner;

public class Producer implements Runnable {

    public void run( ) {
        ActiveMQConnectionFactory factory = new ActiveMQConnectionFactory( "tcp://localhost:61616" );
        try {
            Connection conn = factory.createConnection();
            conn.start();
            Session session = conn.createSession( false, Session.AUTO_ACKNOWLEDGE );
            Destination destination = session.createQueue("Dest");
            MessageProducer producer = session.createProducer(destination);
            producer.setDeliveryMode(DeliveryMode.NON_PERSISTENT);

            Scanner scanner = new Scanner(System.in);
            String message;
            while ((message = scanner.nextLine())!=null) {
                TextMessage textMessage = session.createTextMessage(message);
                producer.send(textMessage);
            }
            session.close();
            conn.close();
        } catch ( JMSException e ) {
            e.printStackTrace( );
        }


    }
}
