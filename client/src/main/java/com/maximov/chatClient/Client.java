package com.maximov.chatClient;

public class Client {
    public static void main(String[] args) {
        Thread thread1 = new Thread(new Consumer());
        thread1.start();
        Thread thread2 = new Thread(new Producer());
        thread2.start();
    }
}
