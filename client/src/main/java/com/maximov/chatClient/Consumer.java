package com.maximov.chatClient;

import org.apache.activemq.ActiveMQConnectionFactory;

import javax.jms.*;

public class Consumer implements Runnable {
    public void run() {
        ActiveMQConnectionFactory factory = new ActiveMQConnectionFactory( "tcp://localhost:61616" );
        Connection conn = null;
        Session session = null;
        try {
            conn = factory.createConnection();
            conn.start();
            session = conn.createSession( false, Session.AUTO_ACKNOWLEDGE );
            Destination destination = session.createTopic("Chat");
            MessageConsumer consumer = session.createConsumer(destination);
            while (true) {
                Message message = consumer.receive();
                System.out.println(((TextMessage) message).getText());
            }
        } catch ( JMSException e ) {
            e.printStackTrace( );
        } finally {
            clean(conn, session);
        }
    }

    private void clean(Connection conn, Session session) {
        try {
            if (session != null)
                session.close();
            if (conn != null)
                conn.close();
        }catch ( JMSException e ) {
            e.printStackTrace( );
        }
    }
}
