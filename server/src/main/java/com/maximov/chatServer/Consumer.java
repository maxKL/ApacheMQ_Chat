package com.maximov.chatServer;

import org.apache.activemq.ActiveMQConnectionFactory;

import javax.jms.*;

public class Consumer implements Runnable {
    public void run() {
        ActiveMQConnectionFactory factory = new ActiveMQConnectionFactory( "tcp://localhost:61616" );
        Connection conn = null;
        Session session = null;
        try {
            conn = factory.createConnection();
            conn.start();
            session = conn.createSession( false, Session.AUTO_ACKNOWLEDGE );
            Destination destination = session.createQueue("Dest");

            Destination topic = session.createTopic("Chat");
            MessageProducer producer = session.createProducer(topic);

            MessageConsumer consumer = session.createConsumer(destination);
            while (true) {
                Message message = consumer.receive();
                System.out.println(((TextMessage) message).getText());
                producer.send(message);
            }
        } catch ( JMSException e ) {
            e.printStackTrace( );
        } finally {
            clean(conn, session);
        }
    }

    private void clean(Connection conn, Session session) {
        try {
            if (session != null)
                session.close();
            if (conn != null)
                conn.close();
        } catch ( JMSException e ) {
            e.printStackTrace( );
        }
    }
}
